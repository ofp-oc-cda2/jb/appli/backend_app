<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
* @Vich\Uploadable
**/
#[ORM\Entity(repositoryClass: NoteRepository::class)]
#[ApiResource]

class Note
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $begining;

    #[ORM\Column(type: 'datetime')]
    private $end;

    #[ORM\Column(type: 'string', length: 50)]
    private $title;

    #[ORM\Column(type: 'string', length: 255)]
    private $content;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private $creator;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $priority;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'notes')]
    private $category;

    #[ORM\OneToMany(mappedBy: 'note', targetEntity: Notification::class, orphanRemoval: true)]
    private $notifications;



    
    #[ApiProperty(iri: 'http://schema.org/contentUrl')]
    #[Groups("read")]
    public ?string $contentUrl = null;

    /** 
    *      @Vich\UploadableField(mapping="user_images", fileNameProperty="filePath")
    **/
    #[Groups("write")]
    public ?File $file = null;

    #[ORM\Column(nullable: true)] 
    public ?string $filePath = null;



    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBegining(): ?\DateTimeInterface
    {
        return $this->begining;
    }

    public function setBegining(\DateTimeInterface $begining): self
    {
        $this->begining = $begining;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, Notification>
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function addNotification(Notification $notification): self
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications[] = $notification;
            $notification->setNote($this);
        }

        return $this;
    }

    public function removeNotification(Notification $notification): self
    {
        if ($this->notifications->removeElement($notification)) {
            // set the owning side to null (unless already changed)
            if ($notification->getNote() === $this) {
                $notification->setNote(null);
            }
        }

        return $this;
    }
}
